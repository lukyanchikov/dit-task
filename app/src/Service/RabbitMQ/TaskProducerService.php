<?php

namespace App\Service\RabbitMQ;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class TaskProducerService
 * @package App\Service\RabbitMQ
 */
class TaskProducerService
{
    /**
     * @param string $data
     * @param KernelInterface $kernel
     * @return int
     */
    public static function newsProducer(string $data, KernelInterface $kernel)
    {
        $application = new Application($kernel);
        $input = new ArrayInput([
            'command' => 'app:producer',
            'taskData' => $data,
            'flag' => 'api_news',
        ]);
        $output = new NullOutput();
        return $application->doRun($input, $output);
    }
}