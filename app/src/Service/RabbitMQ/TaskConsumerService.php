<?php

namespace App\Service\RabbitMQ;

use DateTime;
use SimpleXMLElement;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class TaskConsumerService
 * @package App\Service\RabbitMQ
 */
class TaskConsumerService
{
    const SITEMAP_DIR = '/public/sitemap.xml';
    /**
     * @var string
     */
    private $projectDir;

    /**
     * TaskConsumerService constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->projectDir = $kernel->getProjectDir();
    }

    /**
     * @param array $msg
     */
    public function writeUrlToSitemap(array $msg): void
    {
        $new = $msg['data'];

        $this->createSitemapIfNotExists();
        $this->writeToXml($new);
    }

    private function createSitemapIfNotExists(): void
    {
        $sitemapDir = $this->projectDir . self::SITEMAP_DIR;
        if (!file_exists($sitemapDir)) {
            $this->createXml();
        }
    }

    /**
     * @param array $new
     */
    private function writeToXml(array $new): void
    {
        $sitemapDir = $this->projectDir . self::SITEMAP_DIR;
        $xml = simplexml_load_file($sitemapDir);

        $url = $xml->xpath('url[@id="' . $new['id'] . '"]');
        if (!empty($url)) {
            if ($new['isActive'] == true && $new['isHide'] == false && new DateTime($new['publishedAt']) <= new DateTime()) {
                $this->updateItem($xml, $url[0], $new);
            } else $this->deleteItem($xml, $url);
        } else $this->addItem($xml, $new);
    }

    /**
     * @return SimpleXMLElement
     */
    private function createXml(): SimpleXMLElement
    {
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset></urlset>');
        $this->writeToFile($xml->asXML());
        return $xml;
    }

    /**
     * @param SimpleXMLElement $xml
     * @param $new
     */
    private function addItem(SimpleXMLElement $xml, $new): void
    {
        $url = $xml->addChild('url');
        $url->addAttribute('id', $new['id']);
        $url->addChild('loc', 'http://localhost/api/news/' . $new['slug']);
        $url->addChild('lastmod', $new['updatedAt']);
        $this->writeToFile($xml->asXML());
    }

    /**
     * @param SimpleXMLElement $xml
     * @param SimpleXMLElement $old
     * @param array $new
     */
    private function updateItem(SimpleXMLElement $xml, SimpleXMLElement $old, array $new): void
    {
        $old->loc = 'http://localhost/api/news/' . $new['slug'];
        $old->lastmod = $new['updatedAt'];
        $this->writeToFile($xml->asXML());
    }

    /**
     * @param $xml
     * @param $item
     */
    private function deleteItem($xml, $item): void
    {
        unset($item[0][0]);
        $this->writeToFile($xml->asXML());
    }

    /**
     * @param string $xml
     */
    private function writeToFile(string $xml): void
    {
        file_put_contents($this->projectDir . '/public/sitemap.xml', $xml);
    }
}
