<?php

namespace App\Service\Rest;

use App\Entity\News;
use App\Service\RabbitMQ\TaskProducerService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class AdminService
 * @package App\Service\Rest
 */
class NewsService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    private $kernel;
    private $paginator;

    /**
     * AdminService constructor.
     * @param EntityManagerInterface $em
     * @param KernelInterface $kernel
     * @param PaginatorInterface $paginator
     */
    public function __construct(EntityManagerInterface $em, KernelInterface $kernel, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        $this->paginator = $paginator;
    }

    /**
     * @param array $content
     * @return string
     * @throws Exception
     */
    public function createNew(array $content): string
    {
        $new = new News();
        $this->setFields($new, $content);
        $this->flushEntity($new);
        return "The New {$content['title']} created";
    }

    /**
     * @param News $new
     * @param array $content
     * @return News
     * @throws Exception
     */
    private function setFields(News $new, array $content): News
    {
        return $new->setTitle($content['title'])
            ->setDescription($content['description'])
            ->setShortDescription($content['shortDescription'])
            ->setPublishedAt(new DateTime($content['publishedAt']))
            ->setIsActive($content['isActive'])
            ->setIsHide($content['isHide']);
    }

    /**
     * @param News $new
     */
    private function flushEntity(News $new): void
    {
        $this->em->persist($new);
        $this->em->flush();
        $this->sendNewByMq($new);
    }

    /**
     * @param News $new
     */
    private function sendNewByMq(News $new): void
    {
        $serializer = $this->kernel->getContainer()->get('jms_serializer');
        TaskProducerService::newsProducer($serializer->serialize($new, 'json'), $this->kernel);
    }

    /**
     * @param int $id
     * @param array $content
     * @return string
     * @throws Exception
     */
    public function updateNew(int $id, array $content): string
    {
        /**
         * @var News $new
         */
        $new = $this->em->getRepository(News::class)->find($id);
        if (null != $new) {
            $this->setFields($new, $content)
                ->setUpdatedAt(new DateTime());
            $this->flushEntity($new);
            return "The New $id updated";
        }
        throw new NotFoundHttpException("The New $id does not exist");
    }

    /**
     * @param int $id
     * @return string
     */
    public function deleteNew(int $id): string
    {
        /**
         * @var News $new
         */
        $new = $this->em->getRepository(News::class)->find($id);
        if (null != $new) {
            $new->setIsActive(false);
            $this->flushEntity($new);
            return "The New $id deleted";
        }
        throw new NotFoundHttpException("The New $id does not exist");
    }

    /**
     * @param int $limit
     * @param int $page
     * @return mixed
     */
    public function getNewsList(int $limit, int $page)
    {
        return $this->paginator->paginate(
            $this->em->getRepository(News::class)->queryFindNewsList(),
            $page,
            $limit
        );
    }

    /**
     * @param string $slug
     * @return object
     */
    public function getNew(string $slug)
    {
        $new = $this->em->getRepository(News::class)->findNew($slug);
        if (!is_null($new)) {
            return $new;
        }
        throw new NotFoundHttpException("The New does not exist");
    }
}
