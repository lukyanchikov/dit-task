<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class News
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 * @ORM\Table(name="news")
 */
class News
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="id")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="title")
     */
    private $title;

    /**
     * @var string
     * @Gedmo\Slug(fields={"title"}, unique=true)
     * @ORM\Column(type="string", name="slug", unique=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="text", name="description")
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="string", name="shortDescription")
     */
    private $shortDescription;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="createdAt")
     */
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="updatedAt")
     */
    private $updatedAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", name="publishedAt")
     */
    private $publishedAt;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="isActive", options={"default": true})
     */
    private $isActive;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="isHide", options={"default": true})
     */
    private $isHide;

    /**
     * @var int
     * @ORM\Column(type="integer", name="hits", options={"default": 0})
     */
    private $hits;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
        $this->hits = 0;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return News
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return News
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     * @return News
     */
    public function setShortDescription(string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return News
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getPublishedAt(): DateTime
    {
        return $this->publishedAt;
    }

    /**
     * @param DateTime $publishedAt
     * @return News
     */
    public function setPublishedAt(DateTime $publishedAt): self
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return News
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHide(): bool
    {
        return $this->isHide;
    }

    /**
     * @param bool $isHide
     * @return News
     */
    public function setIsHide(bool $isHide): self
    {
        $this->isHide = $isHide;
        return $this;
    }

    /**
     * @return int
     */
    public function getHits(): int
    {
        return $this->hits;
    }

    /**
     * @param int $hits
     * @return News
     */
    public function setHits(int $hits): self
    {
        $this->hits = $hits;
        return $this;
    }
}
