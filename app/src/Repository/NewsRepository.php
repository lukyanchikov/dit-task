<?php

namespace App\Repository;

use App\Entity\News;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;

/**
 * Class NewsRepository
 * @package App\Repository
 */
class NewsRepository extends EntityRepository
{
    /**
     * @return Query
     */
    public function queryFindNewsList()
    {
        return $this->getEntityManager()
            ->createQuery('
            SELECT n FROM App\Entity\News n 
            WHERE n.isActive = true AND n.isHide = false AND n.publishedAt <= :now
            ORDER BY n.publishedAt DESC
            ')
            ->setParameter('now', new DateTime());
    }

    /**
     * @param string $slug
     * @return ?News
     * @throws NonUniqueResultException
     */
    public function findNew(string $slug): ?News
    {
        return $this->getEntityManager()
            ->createQuery('
                    SELECT n FROM App\Entity\News n 
                    WHERE n.slug = :slug AND n.isActive = true AND n.publishedAt <= :now
                    ')
            ->setParameter('now', new DateTime())
            ->setParameter('slug', $slug)
            ->getOneOrNullResult();
    }
}
