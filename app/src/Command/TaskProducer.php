<?php

namespace App\Command;

use App\Entity\News;
use Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TaskProducer
 * @package App\Command
 */
class TaskProducer extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:producer')
            ->setDescription('producer-description')
            ->addArgument('taskData', InputArgument::REQUIRED)
            ->addArgument('flag', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $message = [];
        $serializer = $this->getContainer()->get('jms_serializer');
        $inputMsg = $serializer->deserialize($input->getArgument('taskData'), News::class, 'json');
        $flag = $input->getArgument('flag');
        if ($flag === 'api_news') {
            $message = [
                "flag" => $flag,
                "data" => [
                    "id" => $inputMsg->getId(),
                    "slug" => $inputMsg->getSlug(),
                    "updatedAt" => $inputMsg->getUpdatedAt(),
                    "publishedAt" => $inputMsg->getPublishedAt(),
                    "isActive" => $inputMsg->isActive(),
                    "isHide" => $inputMsg->isHide(),
                ]
            ];
        }
        $rabbitMessage = $serializer->serialize($message, 'json');

        $this->getContainer()->get('old_sound_rabbit_mq.task_producer')->setContentType('application/json');
        $this->getContainer()->get('old_sound_rabbit_mq.task_producer')->publish($rabbitMessage);
    }
}