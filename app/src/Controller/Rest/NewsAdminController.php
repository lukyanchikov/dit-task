<?php

namespace App\Controller\Rest;

use App\Service\Rest\NewsService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package App\Controller
 */
class NewsAdminController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("/create/news", name="api_get_create_news")
     * @param NewsService $service
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function createNew(NewsService $service, Request $request): JsonResponse
    {
        $content = json_decode($request->getContent(), true);
        return new JsonResponse($service->createNew($content));
    }

    /**
     * @Rest\Post("/update/news/{id}", name="api_post_update_news")
     * @param NewsService $service
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function updateNew(NewsService $service, int $id, Request $request): JsonResponse
    {
        $content = json_decode($request->getContent(), true);
        return new JsonResponse($service->updateNew($id, $content));
    }

    /**
     * @Rest\Delete("/delete/news/{id}", name="api_post_delete_news")
     * @param NewsService $service
     * @param int $id
     * @return JsonResponse
     */
    public function deleteNew(NewsService $service, int $id): JsonResponse
    {
        return new JsonResponse($service->deleteNew($id));
    }
}
