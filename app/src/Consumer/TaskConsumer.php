<?php

namespace App\Consumer;

use App\Service\RabbitMQ\TaskConsumerService;
use Exception;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class TaskConsumer
 * @package App\Consumer
 */
class TaskConsumer implements ConsumerInterface
{
    /**
     * @var TaskConsumerService
     */
    private $service;

    /**
     * TaskConsumer constructor.
     * @param TaskConsumerService $service
     */
    public function __construct(TaskConsumerService $service)
    {
        $this->service = $service;
    }

    /**
     * @param AMQPMessage $msg
     * @return mixed|void
     * @throws Exception
     */
    public function execute(AMQPMessage $msg)
    {
        $response = json_decode($msg->body, true);
        var_dump($response);
        if ($response['flag'] == 'api_news') {
            $this->service->writeUrlToSitemap($response);
        }
    }
}